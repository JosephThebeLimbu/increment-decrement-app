import React from 'react';


const MenuCard = ({ menuData }) => {
    console.log('THE MENU-DATA OR SAY MENUCARD-DATA THAT WE OBTAINED FORM RESTURANT.JS IS', menuData)
    return (
        <>
            <section className='main-card--cointainer' >
                {menuData.map((curElem) => {
                    const { id, name, category, image, description } = curElem; ///<<<--- here we are doing object destructuring so we donot have to repeat curElem every now and oftern . now below this line of code we can write only id instead of curElem.id
                    return (
                        <>
                            <div className='card-container' key={id}>
                                <div className='card'>
                                    <div className='card-body'>
                                        <span className="card-number card-circle subtle">{id}</span>
                                        {/* tala ko dui ota lines of code ma react ma use garine inline css ra component ko through bata use garine css method lai demonstrate gariyeko xa */}
                                        {/* <span className='card-author sublte' style={{ color: 'red' }}>Breakfast</span>
                        <span className='card-author sublte' style={myStyle}>Breakfast</span> */}
                                        <span className='card-author sublte'>{category}</span>
                                        <h2 className='card-title'>{name}</h2>
                                        <span className='card-description subtle'>
                                            {curElem.description}
                                        </span>
                                        <div className='card-read'>Read</div>
                                    </div>
                                    <img src={image} alt="Loading" />
                                    <span className='card-tag subtle'>Order Now</span>

                                </div>
                            </div>
                        </>
                    )

                })}
            </section>


        </>
    )
}

export default MenuCard
