import React, { useState } from 'react' /// yo line of code lekhda ni hunxa ya nalekhda ni hunxa /// since our react version is >=17
import "./style.css"; /// importing css file
import Menu from './menuApi'; ///food haru ko api
import MenuCard from './MenuCard';
import { type } from '@testing-library/user-event/dist/type';
import { Navbar } from './Navbar';



/// yesari export const functionName  chai export default functionName ko satto ma garinxa hai ta   ra jaba yo syntax follow garinxa taba cahi arko import gareko component ma import {} form '' garnu parxa . bhannu ko matlam curly braces bhitra functional component ko name lekhnu parxa hai ta
// export const Resturant = () => {
//     return (
//         <>
//             <h1>hello resturnat</h1>
//         </>
//     )
// }



const uniqueList = [...new Set(Menu.map((curElem) => {
    return curElem.category;
})), 'All']
console.log('uniqueList', uniqueList) ///object



///>>> example of new Set() method and spred operator  <<<
const joseph = [3, 3, 3, 6, 6, 6, 9, 9, 9]
console.log(new Set(joseph))  /// {3,6,9}  /// only unique elements of array are returned by new set() method and the returned result is in object format.
console.log(typeof (new Set(joseph))) /// object
/// now to convert above object into array we will use spread operator and it is shown below:
const newArr = [...new Set(joseph)];  /// [...] <<<--- spread operator
console.log('newArr', newArr); ///--->>>newArr  [3, 6, 9]
console.log('type of newArr is', typeof (newArr));  ///--->>>type of newArr is object  /// though the output is array the console still printed newArr type as an object









/// resturant component definition
export const Resturant = () => {



    /// we can create style component too
    const myStyle = {
        color: 'Green'
    }
    /// end of style component similer to functional component



    /// useState hooks use
    const [menuData, setmenuData] = useState(Menu); /// upState hooks to capture data from Menu api
    const [menuList, setmenuList] = useState(uniqueList);


    const filterItem = (categori) => {
        if (categori === 'All') {
            return setmenuData(Menu);
            //return chai garnai parxa hai kinaki filterItem ta euta method honi ta

        }
        const updatedList = Menu.filter((curElem) => {
            return curElem.category === categori;
        });
        setmenuData(updatedList);
    }



    return (
        <>
            <Navbar filterItem={filterItem} menuList={menuList} />
            <MenuCard menuData={menuData} />
        </>
    )
}
export default Resturant
