import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

import reactDom from 'react-dom';



/// JSX expressions haru lai array le wrap garda chai <div id="root"> bhitra JSX expressions haru warp hunxa hai ta.
// ReactDOM.render(
//   [
//   <h1>
//     hello joseph 9999999999
//     </h1>,
//     <p>joseph thebe 369</p>,
//     <h2>
//       ram 999999999
//     </h2>
//   ],
//   document.getElementById('root')
// );



/// JSX expressions haru lai div le wrap gareko xa bhane chai <div id="root"> bhitra arko <div> pani banxa jasko bhitra chai JSX expression haru wrap bhako hunxa hai ta.
///ReactDOM.render(
///   <div>
///   <h1>
///     hello joseph 9999999999
///     </h1>
///     <p>joseph thebe 369</p>
///     <h2>
///       ram 999999999
///     </h2>
///   </div>,
///   document.getElementById('root')
/// );



/// use of React.Fragment
///<React.Fragment>...</React.Fragment> bhitra JSX expressions haru lai wrap garda kheri chai extra node or say extra div bandaina inside <div id-"root"> ma jasle garda code execution faster huxna.
/// ReactDOM.render(
///    <React.Fragment>
///    <h1>
///      hello joseph 9999999999
///      </h1>
///      <p>joseph thebe 369</p>
///      <h2>
///        ram 999999999
///      </h2>
///    </React.Fragment>,
///    document.getElementById('root')
///  );



/// React.Fragment with syntactic sugar <></>
//  ReactDOM.render(
//     <>
//     <h1>
//       hello joseph 9999999999
//       </h1>
//       <p>joseph thebe 369</p>
//       <h2>
//         ram 999999999
//       </h2>
//     </>,
//     document.getElementById('root')
//   );



/// JSX challenge /// rendering netflix five series 
// ReactDOM.render(
//   <>
//     <h1>
//       My Fav. Netflix Series
//     </h1>
//     <p>these sre my fav. seies</p>
//     <ol>
//       <li>dark</li>
//       <li>vikings</li>
//       <li>breaking bad</li>
//     </ol>
//   </>,
//   document.getElementById('root')
// );



/// original React app ma hune index.js ko line of code chai tala ko ho lah.
reactDom.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)









