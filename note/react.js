///React
/// it is a javaScript library for building user interfaces.
/// the user interface(UI) is the point of human-computer interaction and communication in a device . this can include display screens , keyboards , a mouse and the apperance of a desktop.
/// desktop ma dekhina j pani ui part or say front end part teslai hami user interface bhanxum jun use garera user le hamro website sangha interact garna sakos.



/// react is not a framework . it is a js library for building user interafaces .
/// react or reactJS or react.js
/// react knows only one thing i.e. to create awsome user interfaces



/// react history
/// designed by Jorden Wale , a software engineer at facebook.
/// it was first delplyoed for facebook news feed around 2011
/// in 2013 , react was open sourced at js conference.



///What is difference between package json and package lock json?
///The package.json is used for more than dependencies - like defining project properties, description, author & license information, scripts, etc. The package-lock. json is solely used to lock dependencies to a specific version number.



/// react app ko default port number >>> http://locahost:3000/ or localhost:3000
/// http://localhost:3000/



///about react
/// it follows component based approach.
/// a component is one of the core building blocks of React.
/// in other words , we can say that every application you will develop in React will be made of pieces called components . Components make the task of building UIs much easier.
/// we can say a component is a piece of code .
/// component based bhako huna le yedi same code code chaiyeko xa dherai thau ma bhane tyo code lai component consider garera barambar tyo component  use garna sakiyo.



/// React uses a Declarative Approach
/// Declarative Programming is like asking your friend to draw a landscape . You don't care how they draw it , thats up to them.
/// client lai actual requirement taha hudaena . tara pani yedi client le euta ecommerce website banaidinu hosh bhanyo bhane timi le as a React developer euta esto website banaidinxau jasma euta ecommerce website hune sapai basic kura haru hunxa .
/// client and developer duwai jana khusi hune xa.



///in React DOM updates are handled gracefully.



/// React is designed for speed , speed of implementing the application simplicity and scalability.



/// why use React ?
/// it is created and maintained by Facebook . so it is very very famous .
/// it has a huge community on GitHub.
/// component based achitecture .



/// install extension : JavaScript (ES6) code snippets
/// VScode icons
/// node.js install garda kheri NPM automatically installed hunxa hai ta .



///React installation procses .
/// > install NodeJS and NPM
/// > install VSCode or any IDE
/// > install react from terminal
      /// syntax : npm install -g create-react-app
      ///          create-react-app --version
      ///          create-react-app <projectName>



/// creat-react-app <project_name>
/// react ma j kam gare pani react app bhanne chalan xa hai ta .



/// cmd shorcut : if in c to go to d just use command d:
/// mkdir dir_name
/// cd dir_name /// use tab to get suggesation of directory name



///package.json ma tyo React app ko meta data hunnxa hai
/// tyo React app k ma dependent xa bhanne kura ni yesma lekheko hunxa hai ta
/// exaple of some dependencies are :
///                            "react": "^17.0.2",
///                            "react-dom": "^17.0.2",
///                            "react-scripts": "4.0.3",
/// code ma yo dependencies haru lai use garinxa as shown below:
///                             import React from 'react';
///                             import ReactDOM from 'react-dom';



/// vscode ma rafce lekhda tala ko kura haru automatically auxa
/// import React from 'react'

/// export const App = () => {
///   return (
///     <div>

///     </div>
///   )
/// }



/// as a beginner ma package-lock.json is not needed hai



/// manifest.json file provides the imformation of our application



///src bhitra bhako app.js chai euta component nae ho



/// src bhitra bhako index.js chai main file ho jasma ma hami hamro react app ko pura code lekhne wala xum.



/// public bhitra bhako manifest.json file ma hamro react application ko description hunxa in a json format.
/// public bhitra bhako index.html react app ma hune eutai matra index.html ho jasma hami khasai kehi changes gardaenam . react app banauna j suru ma deko hunxa last sammha tehi nae rakhinxa hai ta.



///favicon.ico chai top ma bhako sano image ho hai



///robots.txt file chai SEO ko lagi use garinxa hai
///A robots. txt file tells search engine crawlers which URLs the crawler can access on your site.



///
///<noscript></noscript> element is used if the user has disabled the javascript or his browser does not support javascript then it shows what it is there inside noscript element . usually noscript element has content as You need to enable javascript to run this app.



/// <div id="root"></div>
/// inside body of index.html we have only one element div with id="root" as shown above . using this element we will build our react app . and this is called single page application.



/// jaba hami var React=require('react') garxam ni taba hami le babel pani hamro react app ma automatically pauxam hai ta .
/// webpack le hamro sapai files ra folder haru lai bundle banayera rakhne kam gardaxa
/// jaba hami npm install -g react-app command fire garim ni taba nai babel ra webpack automatically download hunxa hai ta



/// babel is basically a compiler of javaScript
/// since we have morden js i.e ecma script that cannot be directly undrestood by browser
/// babel compiles morden js code in such a js format that can be undrestood by browsers .



/// babel automatically download bhako huna le hami le react app ma morden js code or ES6 feature use bhako code lekhna saknxam kinaki babel le tyo code lai browser le bujne js code ma convert gardinxa hai .
/// import export chai mordern js ma ako feature ho hai ta jun chai require ko sato use garinxa aja bholi



///ReactDOM.render(<h1>hello world!!!!!!!!!</h1>,document.getElementById('root'))
/// in above line of code the data <h1> element is called JSX.



///Introduction to JSX in react
/// JSX meaning is javaScript extensions or javScript XML
///react-dom ko render method bhitra lekhina html expression lai nai JSX bhaninxa hai .



/// yedi react module import nagari JSX use gariyo bhane error auxa hai . bhannu ko matlab yedi react application ma JSX use garinxa bhane tyo case ma react module import garekai hunu parxa hai ta .
///
/// JSX , react , react-dom ko bich ma ganistha samandha xa hai .



///ReactDOM.render(<h1>hello world!!!!!!!!!</h1>,document.getElementById('root'))
/// mathi ko line of code lai babel le kasari transform garxa  bhanera tala show gareko xa hai ta.
///
///
///
///modern raeact code without babel>>>ReactDOM.render(<h1>hello world!!!!!!!!!</h1>,document.getElementById('root'))
///
///
///
///babel le convert gareko mathi ko js code>>>ReactDom.render(React.createElement("h1",null,"hello world!!!!!!!!!")),document.getElementById('root')
///
///
///
/// React module ko use bhako kura babel le convert gareko ko code ma dekhinxa , jasle garda k taha hunxa bhane yedi react app ma jsx use garne ho bhane react module ko k kam xa bhanera .



/// one of the important concept that we forget often but is of great importance and to be undrestood thoroughly:

///   var h1 = document.createElement('h1');
///   h1.innerHTML('hello world 999333666'); /// wrong way
///   document.getElementById('root').appendChild(h1)
/// >>> the above line of code will give error saying that TypeError: h1.innerHTML is not a function
/// >>> we got that error beacuse h1 here is a javaScript variable which holds html element . it isnt a html element by itself beacause it as javacript variable hence the innerHTML cannot be used her .



///the following code is correct working solution to the problem we saw above . to use innerHTMl in a variable holding html element we use = or assignment operator
///   var h1 = document.createElement('h1');
///   h1.innerHTML="hello world 99933666"; /// correct way
///   document.getElementById('root').appendChild(h1)



/// render multiple elements inside ReactDOM.render() in ReactJS
/// ReactDOM.render('k dekaune','kaha dekhaune',callBackFunction())
/// yedi render() method bhitra multiple elements use garne ho bhane we should wrap adjacent JSX elements inside an eclosing tag.
/// render() method le euta matra JSX element support garxa hai ta so yedi tannai JSX element use garna man xa bhane tyo sapai JSX elements haru lai euta outer covering JSX element bhitra rakhnu parxa
///                >>> adjacent JSX elements wrapped inside a div element or enclosing div tag >>>   ReactDOM.render(<div><h1>hello world!!!!!!!!!</h1>,<p>hello ram 999</p></div>,document.getElementById('root'))
/// for react version more than 16 instead of wrapping JSX elements inside a outer element or say div or any tag we can place all the JSX elements inside an arra [] and seperate each JSX expression using comma.



///React fragment in React js
/// <React.Fragment>...</React.Fragment> bhitra JSX expressions haru lai wrap garda kheri chai extra node or say extra div bandaina inside <div id-"root"> ma jasle garda code execution faster huxna.
/// <React.Fragment> ko syntactic sugar chai <> yo ho la . so instead of writing <React.Fragment>...</React.Fragment> we can write only <>...</>



/// array le JSX lai wrap garda pani extra div-node inside <div id="root"> ma bandaena.



/// div le JSX lai wrap garda chai extra div-node banxa inside root div.



// ReactDom.render(
//       <p>

//       </p>
//       ,document.getElementById("root")
// )



/// date 13/01/2022 /// re-continuing React course /// 369
/// npx create-react-app app_name ;  /// this synatax will target specific folder rather than global .



/// react app ma public folder lai 90% time hami kahile modify gardaem ya chudaenam .
/// j jati kam garinxa tyo source folder ma garinxa hai react app ma .



/// 2 of the important extensions to be downloaded for React app:-
/// JavaScript (ES6) code snippets
/// ES7 React/Redux/GraphQL/React-Native snippets



/// rafce meaning : react arrow function component with export
/// export default App <<<--- yo line of code last ma lekhna nabirsine hai kahile pani



/// functional component ko basic syntax tala xa hai :
/// export const App = () => {
///       return (
///             <div>
///                   welcome to joseph 369
///             </div>
///       )
/// }



/// yedi react version 17 xa ya 17 bhanda jyada xa bhane chai ' import React from 'react' ' bhanera lehirakhunu pardaena hai ta.



/// <section></section> tag bhitra pani JSX haru lai warp garna sakinxa hai ta .



/// JSX ko rule haru tala xan:
/// JSX elements haru lai wrap garnu parxa.
/// use camelCase for attrinutes of JSX .example :JSX element or tag ma class assign garda kheri class haina className lekhnu parxa hai ta kinaki class already html ko attribute ho ra html ra JSX ma difference xa .
/// JSX ma sapai tag or say element haru closed bhako hunu parxa hai . for example image tag praya close garidaena tara in react we should close all the tags in JSX expression.
/// ReactDom.render() bhitra bhako html tag haru lai JSX bhaninaxa .



/// "emmet.includeLanguages": {"javascript": "javascriptreact"}
/// mathi ko line of code chai settings bhitra ko settings.json ma lagera paste garda kheri chai autocomplete facilty auxa JSX ma . example JSX area ma h type garna sath h1,h2,h3 options  haru dekhinxa hai ta.



///JSX ko main faida bhaneko chai yesma hami le javaScript use garna sakxam though yo  html expression kae format ma xa.
/// example:
/// export const App = () => {
///       return (
///             <div>
///                   welcome to joseph 369 {3 + 3 + 3}  /// yaha {3+3+3} chai 9 bhaidinxa output ma
///             </div>
///       )
/// }



/// export default component_or_functionalComponentName
/// yedi default pani lekheko xa bhane chai import garda kheri .js lekhirakhnu pardae exampla import App from './App'



///Continuous integration (CI) and continuous delivery (CD)



/// normla html ma inline css use garne tarika >>> <h1 style="border: 2px solid Tomato;">Hello World</h1>
/// react ma use garine inline css ko tarika >>>  <span className='card-author sublte' style={{ color: 'red' }}>Breakfast</span>
/// double curly braces bhitra rakhne exact css code lai .



/// img tag in react.js
///   <img src="" alt="" /> /// src ma direct image ko google bata link ni rakhna milxa inside curly braces e.g. src={} or we can keep the route of the image that is present in the project.



///React Hooks:
/// 1st hooks >>> useState hooks ko kam bhaneko chai kunai pani data lai hold garera or say store garera rakhne .
/// useState(initialdata) hooks has a parameter 'initialdata'(name can be anything). useState(initialdata) returns an array of two elements. the first elmenet that the array contains is 'state variable'(name can be anaything) which store or holds the data or api . the element in the array is updated data .
/// react app ma data lai manage garna ko lagi use garine hooks lai useState() hook bhaninxa.
/// useState() le react app ma hune data or state lai manage garne kam gardaxa hai ta
/// yehi useState() hooks use garera react app ko data lai manage garne process lai state management bhaninxa hai ta.



/// trsnsfer Data from one component to another componet using props
/// resturant.js ma menu api ko data xa jun chai resturant.js le MenuCard.js lai transfer garnu parne xa
/// <MenuCard menuData={menuData}/>   in Resturant.js
/// resturant component definition
/// example of prop in tarnsfering data from one component to another : here parent component is Resturant , child component is MenucCard and the data is Menu
// export const Resturant = () => {
//       const [menuData, setmenuData] = useState(Menu)
//       return (
//             <>
//                   <MenuCard menuData={menuData} />   /// yaha chai MenuCard ma attribute menuData diyera Menu data lai pass gariyeko xa /// yesari tag ma curly brances use garera attribute dina lai prop bhaninxa hai ta

//             </>
//       )
// }
// export default Resturant



/// import React, { useState } from 'react' ; /// useState() hooks lai use garna react bata teslai import garnu parxa hai ta
/// React.useState(Menu) garne ho bhane mathi React import gareko hunu parxa
/// useState(Menu) matra use garna ho bhane React,{useState} duitai import gareko hunu parxa individullay hai ta



/// react v17 ma ko problem bhaneko chai save garda kheri page relod nahune hai ta.



/// new Set() method will returns an object containing only unique elements



/// spred oprator [...object] use gaera object lai array ma convert garna sakinxa



/// map((curElem,index,arrya)=>{})    map method



/// filter((curElem,index,array)=>{})



/// react v17 ma hot reloading problem xa , matlab save garera reload garepaxi matra changes apply bhako dekhinxa hai ta  >>>



/// >>> working of img tag >>>  <img src={image} alt="Loading" /> <<<--------- yo tag le public folder bhitra ko image folder lai hit garxa hai ta .
/// >>> image: "/images/maggi.jpg"
/// >>> image: "../images/maggi.jpg"
/// >>> image: "./images/maggi.jpg",
/// >>> image: "/images/maggi.jpg",
/// >>> image: "images/maggi.jpg",
/// >>> all the above syntax are corect . the img tag will hit src image folder present in public folder for allt the syntax present above.



/// how to create a ssh key for gitlab
/// open git bash and run following commands:
/// ssh-keygen  then enter enter   output auxa--------->>> Your public key has been saved in C:\Users\cicir/.ssh/id_rsa.pub.
/// cat /C/Users/cicir/.ssh/id_rsa.pub.   <<<--------- yo code run garisake paxi temro ssh key show hunxa



/// react hooks le chai redux lai replace gardae xa hai ta



///--------->>>cosnt[myNum,setmyNum]=useState[initialData]  /// yaha setmyNum cahi euta esto function ho jasle return garreko output chai myNum ko current value hunxa hai ta






































































































